#!/bin/bash

#push image version 99.99 to docker hub
docker build -t brentongarrett0/gci-vm-name-deciphering-tool-backend:99.99 .
docker image push brentongarrett0/gci-vm-name-deciphering-tool-backend:99.99
echo -e "\e[32mdecipher-tool backend image version 99.99 pushed to docker hub \e[39m"

#push image version 99.99 as latest to docker hub
docker build -t brentongarrett0/gci-vm-name-deciphering-tool-backend:latest .
docker image push brentongarrett0/gci-vm-name-deciphering-tool-backend:latest
echo -e "\e[32mdecipher-tool backend image version 99.99 pushed as latest to docker hub \e[39m"

#Remote into the remote host, prep the environment
ssh $DOCKER_SERVER_USERNAME@$DOCKER_SERVER_URL << EOF
#change context to to the home dir
cd /home/$DOCKER_SERVER_USERNAME

#if directory called ‘decipher-tool-backend-app’ exists, delete it
if [ -d "decipher-tool-backend-app" ]; then
    rm -rf decipher-tool-backend-app
    echo -e "\e[32mdecipher-tool-backend-app directory deleted! \e[39m"
fi

#Create a new directory called ‘decipher-tool-backend-app’
mkdir decipher-tool-backend-app
echo -e "\e[32mdecipher-tool-backend-app dir created! \e[39m"
EOF

#Scp the app file from local machine to 'decipher-tool-backend-app' on the remote host
scp docker-compose.yaml $DOCKER_SERVER_USERNAME@$DOCKER_SERVER_URL:/home/$DOCKER_SERVER_USERNAME/decipher-tool-backend-app
echo -e "\e[32mdocker-compose.yaml copied to remote host \e[39m"

#Run docker-compose up -d
ssh $DOCKER_SERVER_USERNAME@$DOCKER_SERVER_URL << EOF
cd /home/$DOCKER_SERVER_USERNAME/decipher-tool-backend-app
docker-compose down
docker-compose pull
docker-compose up -d
docker container ls
EOF