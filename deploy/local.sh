#!/bin/bash

#build project
gradle clean build 

#build image
docker build -t decipher-tool-backend:local .
echo 'container built'

#remove existing container with the same name
docker container stop decipher-tool-backend
echo 'container stopped'

docker container rm decipher-tool-backend
echo 'container removed'

#run new container
docker run -d --name decipher-tool-backend -p 8080:8080 decipher-tool-backend:local
docker container ls