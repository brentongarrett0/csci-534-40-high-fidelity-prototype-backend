package com.gci.vmnamedecipheringtool.services;

import org.json.JSONObject;
import org.springframework.stereotype.Service;
import org.yaml.snakeyaml.Yaml;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

@Service
public class VMNamingConventionService {

    public String getVMNamingConvention() throws FileNotFoundException, IOException {
        //grab file content
        String vmNamingConvention = "";
        try { vmNamingConvention = new String(Files.readAllBytes(Paths.get("config/vm-naming-convention.yaml"))); }
        catch (IOException e) { e.printStackTrace(); }

        //convert vmNamingConvention to json
        Yaml yaml= new Yaml();
        Map<String,Object> map= (Map<String, Object>) yaml.load(vmNamingConvention);
        JSONObject jsonObject=new JSONObject(map);
        return jsonObject.toString();
    }

}
