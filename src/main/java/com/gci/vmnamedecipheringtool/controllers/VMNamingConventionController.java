package com.gci.vmnamedecipheringtool.controllers;

import com.gci.vmnamedecipheringtool.services.VMNamingConventionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import java.io.FileNotFoundException;
import java.io.IOException;

@RestController
public class VMNamingConventionController {

    @Autowired
    private VMNamingConventionService vmnamingconventionservice;

        @GetMapping(path = "/greeting")
        public String greeting(){
            return "hello from the container!";
        }

        @CrossOrigin(origins = {"http://localhost:3001" , "http://52.24.180.139:3001"} , allowedHeaders = "*")
        @GetMapping(path = "/vm-naming-convention")
        public String getVMNamingConvention() throws FileNotFoundException, IOException {
            return vmnamingconventionservice.getVMNamingConvention();
        }
}
