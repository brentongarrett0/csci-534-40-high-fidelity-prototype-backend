FROM openjdk:8-jdk-alpine
LABEL maintainer="brenton@initv.io"
VOLUME /tmp
WORKDIR /deciphering-tool
COPY config/vm-naming-convention.yaml config/vm-naming-convention.yaml
ARG JAR_FILE=build/libs/deciphering-tool-backend-app.jar
COPY ${JAR_FILE} deciphering-tool-backend-app.jar
ENTRYPOINT ["java","-jar"]
CMD ["/deciphering-tool/deciphering-tool-backend-app.jar"]